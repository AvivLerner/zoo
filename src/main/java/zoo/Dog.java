package zoo;
import static program.Main.PRINTER;

public class Dog implements Animal{
    @Override
    public void printName()
    {
        PRINTER.info("Dog");
    }

    @Override
    public void printSound()
    {
        PRINTER.info("How");
    }
}
