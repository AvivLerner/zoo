package zoo;
import exceptions.AnimalNotInZooException;
import exceptions.TryingMakingAnimalObjectException;
import java.lang.reflect.Constructor;

public class AnimalFactory {
    private static final String ROOT = "zoo.";
    private static final String ANIMAL_NOT_IN_ZOO_EXCEPTION_MESSAGE = "Zoo Doesn't Have %s";
    private static final String MAKE_NEW_ANIMAL_EXCEPTION_MESSAGE = "Failed creating a new %s";

    public Animal getAnimal(String animalName) throws AnimalNotInZooException, TryingMakingAnimalObjectException {
        try {
            String fullAnimalName = ROOT + animalName; // change
            Class<?> classObject = Class.forName(fullAnimalName);
            Constructor<?> classConstructor = classObject.getConstructor();
            Object objectInstance = classConstructor.newInstance();
            if (objectInstance instanceof Animal){
                return (Animal)objectInstance;
            }
            else{
                throw new AnimalNotInZooException(ANIMAL_NOT_IN_ZOO_EXCEPTION_MESSAGE.formatted(animalName));
            }
        }
        catch (ClassNotFoundException e)
        {
            throw new AnimalNotInZooException(ANIMAL_NOT_IN_ZOO_EXCEPTION_MESSAGE.formatted(animalName), e);
        }
        catch(Exception otherException)
        {
            throw new TryingMakingAnimalObjectException(MAKE_NEW_ANIMAL_EXCEPTION_MESSAGE.formatted(animalName), otherException);
        }
    }

}
