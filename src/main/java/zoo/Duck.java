package zoo;
import static program.Main.PRINTER;

public class Duck implements Animal{
    @Override
    public void printName()
    {
        PRINTER.info("Duck");
    }

    @Override
    public void printSound()
    {
        PRINTER.info("Ga ga");
    }
}
