package zoo;

public interface Animal {
    public void printName();
    public void printSound();
}
