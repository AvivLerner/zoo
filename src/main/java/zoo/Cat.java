package zoo;
import static program.Main.PRINTER;

public class Cat implements Animal{
    @Override
    public void printName()
    {
        PRINTER.info("Cat");
    }
    @Override
    public void printSound()
    {
        PRINTER.info("Miao");
    }
}
