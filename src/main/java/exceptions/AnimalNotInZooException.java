package exceptions;

public class AnimalNotInZooException extends Exception{
    public AnimalNotInZooException(String message)
    {
        super(message);
    }

    public AnimalNotInZooException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
