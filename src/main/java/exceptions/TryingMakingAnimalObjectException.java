package exceptions;

public class TryingMakingAnimalObjectException extends Exception{
    public TryingMakingAnimalObjectException(String message)
    {
        super(message);
    }

    public TryingMakingAnimalObjectException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public TryingMakingAnimalObjectException(Throwable cause)
    {
        super(cause);
    }
}
