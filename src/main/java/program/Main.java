package program;
import org.slf4j.impl.SimpleLoggerFactory;
import zoo.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    private static final String ANIMALS_LIST_SEPARATOR = " ";
    private static final int ANIMALS_LIST_PATH = 0;
    private static final String NEW_PATH_MESSAGE = "List of animals' path is incorrect. Please enter a new path";
    private static final String WRONG_PATH_LOG = "User gave wrong path";
    private static final SimpleLoggerFactory loggerFactory = new SimpleLoggerFactory();
    public static final org.slf4j.Logger PRINTER = loggerFactory.getLogger("MyPrinter");
    public static final org.slf4j.Logger LOGGER = loggerFactory.getLogger("MyLogger");

    public static void main(String[] args) {
        String verifiedPath = getVerifiedFilePath(args[ANIMALS_LIST_PATH]);
        try {
            Scanner fileReader = new Scanner(new File(verifiedPath));
            String[] animals = fileReader.nextLine().split(ANIMALS_LIST_SEPARATOR);
            for (String animalName : animals) {
                printNameAndSoundOfAnimal(animalName);
            }
        }
        catch(FileNotFoundException ignored){
        }
    }


    public static String getVerifiedFilePath(String originalPath)
    {
        if (new File(originalPath).exists()) {
            return originalPath;
        }
        while(true) {
            LOGGER.info(WRONG_PATH_LOG);
            System.out.println(NEW_PATH_MESSAGE);
            Scanner inputReader = new Scanner(System.in);
            String newPath = inputReader.nextLine();
            if (new File(newPath).exists()) {
                return newPath;
            }
        }
    }

    public static void printNameAndSoundOfAnimal(String animalName)
    {
        AnimalFactory factory = new AnimalFactory();
        try {
            Animal animal = factory.getAnimal(animalName);
            animal.printName();
            animal.printSound();
        } catch (Exception exception)
        {
            PRINTER.info(exception.getMessage());
            LOGGER.debug(exception.getCause().toString());
        }
    }
}

